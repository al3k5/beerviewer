package com.example.alekskrause.beerviewer.adapter;

/**
 * Created by alekskrause on 20/11/2017.
 */

public interface OnBottomReachedListener {

    //main menu callback
    public void onBottomReached(int position);
}
