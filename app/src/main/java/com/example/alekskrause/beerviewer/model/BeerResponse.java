package com.example.alekskrause.beerviewer.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by alekskrause on 16/11/2017.
 */

public class BeerResponse implements Parcelable {

    @SerializedName("results")
    private List<Beer> results;
    @SerializedName("total_results")
    private int totalResults;
    @SerializedName("total_pages")
    private int totalPages;


    public List<Beer> getResults() {
        return results;
    }

    public List<Beer> getBeers() {
        return results;
    }

    public void setResults(List<Beer> results) {
        this.results = results;
    }

    public void setBeers(List<Beer> results) {
        this.results = results;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.results);
        dest.writeInt(this.totalResults);
        dest.writeInt(this.totalPages);
    }

    public BeerResponse() {
    }

    protected BeerResponse(Parcel in) {
        this.results = in.createTypedArrayList(Beer.CREATOR);
        this.totalResults = in.readInt();
        this.totalPages = in.readInt();
    }

    public static final Parcelable.Creator<BeerResponse> CREATOR = new Parcelable.Creator<BeerResponse>() {
        @Override
        public BeerResponse createFromParcel(Parcel source) {
            return new BeerResponse(source);
        }

        @Override
        public BeerResponse[] newArray(int size) {
            return new BeerResponse[size];
        }
    };
}
