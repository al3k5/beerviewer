package com.example.alekskrause.beerviewer.networking;


import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by alekskrause on 15/11/2017.
 */

public class BeerClient {
    public static final String API_PATH = "https://api.punkapi.com/v2/";

    public static Retrofit retrofit = null;

    public static Retrofit getApi(){
        if(retrofit==null){
            //initialize beer service
            retrofit = new Retrofit.Builder()
                    .baseUrl(API_PATH)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
