package com.example.alekskrause.beerviewer.networking;

import com.example.alekskrause.beerviewer.model.Beer;
import com.example.alekskrause.beerviewer.model.BeerResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by alekskrause on 16/11/2017.
 */

public interface BeerService {
    @GET("beers")
    Call<List<Beer>> getBeers(@Query("page") int page);
}
