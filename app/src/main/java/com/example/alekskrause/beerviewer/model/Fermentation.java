
package com.example.alekskrause.beerviewer.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Fermentation implements Parcelable
{

    @SerializedName("temp")
    @Expose
    private Temp temp;
    public final static Parcelable.Creator<Fermentation> CREATOR = new Creator<Fermentation>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Fermentation createFromParcel(Parcel in) {
            return new Fermentation(in);
        }

        public Fermentation[] newArray(int size) {
            return (new Fermentation[size]);
        }

    }
    ;

    protected Fermentation(Parcel in) {
        this.temp = ((Temp) in.readValue((Temp.class.getClassLoader())));
    }

    public Fermentation() {
    }

    public Temp getTemp() {
        return temp;
    }

    public void setTemp(Temp temp) {
        this.temp = temp;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(temp);
    }

    public int describeContents() {
        return  0;
    }

}
