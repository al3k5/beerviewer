package com.example.alekskrause.beerviewer.Utils;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;

import com.example.alekskrause.beerviewer.R;

/**
 * Created by alekskrause on 21/11/2017.
 */

public class Device {
    //checks if device is a phone according to screen size
    public static boolean isMobile(Context context) {
        int smallestWidth;
        Resources resources = context.getResources();

        int smallestWidthConfig = resources.getConfiguration().smallestScreenWidthDp;
        if (smallestWidthConfig == Configuration.SMALLEST_SCREEN_WIDTH_DP_UNDEFINED) {
            float smallestWidthDimens =
                    resources.getDimension(R.dimen.smallestWidth)
                            / resources.getDisplayMetrics().density;
            smallestWidth = (int) Math.ceil(smallestWidthDimens);
        } else {
            smallestWidth = smallestWidthConfig;
        }

        return smallestWidth < 600;
    }

    public static Display getDisplay(Context context){
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        return display;
    }
}
