package com.example.alekskrause.beerviewer.data;

import android.content.Context;
import android.os.AsyncTask;

import com.example.alekskrause.beerviewer.model.Beer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alekskrause on 21/11/2017.
 * Fetches data from db in async manner
 */

public class AsyncTaskDB extends AsyncTask<Void, Void, Void> {
    private OnFinishListener onFinishListener;
    private Context context;
    private ArrayList<Beer> beers;

    public AsyncTaskDB(Context context, OnFinishListener onFinishListener) {
        this.context = context;
        this.onFinishListener = onFinishListener;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        FavoriteDbHelper favoriteDbHelper = new FavoriteDbHelper(context);
        ArrayList<Beer> beers = favoriteDbHelper.getAllFavoriteBeers();
        if (beers != null && !beers.isEmpty()) this.beers = beers;
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if (onFinishListener != null) {
            onFinishListener.onFinished(beers);
        }
    }

    public interface OnFinishListener {
        void onFinished(ArrayList<Beer> beers);
    }
}
