package com.example.alekskrause.beerviewer.data;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.util.Log;

import com.example.alekskrause.beerviewer.model.Amount;
import com.example.alekskrause.beerviewer.model.Beer;
import com.example.alekskrause.beerviewer.model.BoilVolume;
import com.example.alekskrause.beerviewer.model.Fermentation;
import com.example.alekskrause.beerviewer.model.Hop;
import com.example.alekskrause.beerviewer.model.Ingredients;
import com.example.alekskrause.beerviewer.model.Malt;
import com.example.alekskrause.beerviewer.model.MashTemp;
import com.example.alekskrause.beerviewer.model.Method;
import com.example.alekskrause.beerviewer.model.Temp;
import com.example.alekskrause.beerviewer.model.Volume;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alekskrause on 19/11/2017.
 * All database operations. onUpgrade is not used because we have not had a release yet.
 */

public class FavoriteDbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "beer.db";
    private static final int DATABASE_VERSION = 1;
    public static final String LOGTAG = "FAVORITE";
    SQLiteOpenHelper dbHandler;
    SQLiteDatabase db;

    public FavoriteDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void open() {
        Log.i(LOGTAG, "Database opened");
        db = dbHandler.getWritableDatabase();
    }

    public void close() {
        Log.i(LOGTAG, "Database closed");
        dbHandler.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_BEER_TABLE);
        db.execSQL(SQL_CREATE_METHOD_TABLE);
        db.execSQL(SQL_CREATE_MASH_TEMP_TABLE);
        db.execSQL(SQL_CREATE_INGREDIENT_TABLE);
        db.execSQL(SQL_CREATE_MALT_TABLE);
        db.execSQL(SQL_CREATE_HOP_TABLE);
        db.execSQL(SQL_CREATE_FOOD_PAIRING_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //db.execSQL("DROP TABLE IF EXISTS " + FavoriteContract.Beer.TABLE_NAME);
        //onCreate(db);
    }

    public void addFavouriteBeer(Beer beer) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues beerContentValues = new ContentValues();
        beerContentValues.put(FavoriteContract.Beer.COLUMN_SERVER_ID, beer.getId());
        beerContentValues.put(FavoriteContract.Beer.COLUMN_NAME, beer.getName());
        beerContentValues.put(FavoriteContract.Beer.COLUMN_TAGLINE, beer.getTagline());
        beerContentValues.put(FavoriteContract.Beer.COLUMN_FIRST_BREWED, beer.getFirstBrewed());
        beerContentValues.put(FavoriteContract.Beer.COLUMN_DESCRIPTION, beer.getDescription());
        beerContentValues.put(FavoriteContract.Beer.COLUMN_IMAGE_URL, beer.getImageUrl());
        beerContentValues.put(FavoriteContract.Beer.COLUMN_ABV, beer.getAbv());
        beerContentValues.put(FavoriteContract.Beer.COLUMN_IBU, beer.getIbu());
        beerContentValues.put(FavoriteContract.Beer.COLUMN_TARGET_FG, beer.getTargetFg());
        beerContentValues.put(FavoriteContract.Beer.COLUMN_TARGET_OG, beer.getTargetOg());
        beerContentValues.put(FavoriteContract.Beer.COLUMN_EBC, beer.getEbc());
        beerContentValues.put(FavoriteContract.Beer.COLUMN_SRM, beer.getSrm());
        beerContentValues.put(FavoriteContract.Beer.COLUMN_PH, beer.getPh());
        beerContentValues.put(FavoriteContract.Beer.COLUMN_ATTENUATION_LEVEL, beer.getAttenuationLevel());
        beerContentValues.put(FavoriteContract.Beer.COLUMN_VOLUME_TEMPERATURE, beer.getVolume().getValue());
        beerContentValues.put(FavoriteContract.Beer.COLUMN_VOLUME_UNIT, beer.getVolume().getUnit());
        beerContentValues.put(FavoriteContract.Beer.COLUMN_BOIL_VOLUME_TEMP, beer.getBoilVolume().getValue());
        beerContentValues.put(FavoriteContract.Beer.COLUMN_BOIL_VOLUME_UNIT, beer.getBoilVolume().getUnit());
        beerContentValues.put(FavoriteContract.Beer.COLUMN_BREWERS_TIPS, beer.getBrewersTips());
        beerContentValues.put(FavoriteContract.Beer.COLUMN_FAVORITE, Beer.FAVORITE);

        Long beerDbResult = db.insert(FavoriteContract.Beer.TABLE_NAME, null, beerContentValues);
        int beerId = beerDbResult.intValue();

        Method method = beer.getMethod();
        ContentValues methodContentValues = new ContentValues();
        methodContentValues.put(FavoriteContract.Method.COLUMN_BEER_ID, beerId);
        methodContentValues.put(FavoriteContract.Method.COLUMN_FERMENTATION_TEMP, method.getFermentation().getTemp().getValue());
        methodContentValues.put(FavoriteContract.Method.COLUMN_TEMP_UNIT, method.getFermentation().getTemp().getUnit());
        methodContentValues.put(FavoriteContract.Method.COLUMN_TWIST, method.getTwist());

        Long methodDbResult = db.insert(FavoriteContract.Method.TABLE_NAME, null, methodContentValues);
        int methodId = methodDbResult.intValue();

        List<MashTemp> mashTemps = method.getMashTemp();
        ContentValues mashContentValues = new ContentValues();
        for (MashTemp mashTemp : mashTemps) {
            mashContentValues.put(FavoriteContract.MashTemp.COLUMN_METHOD_ID, methodId);
            mashContentValues.put(FavoriteContract.MashTemp.COLUMN_MASH_TEMP, mashTemp.getTemp().getValue());
            mashContentValues.put(FavoriteContract.MashTemp.COLUMN_TEMP_UNIT, mashTemp.getTemp().getUnit());
            mashContentValues.put(FavoriteContract.MashTemp.COLUMN_DURATION, mashTemp.getDuration());
            db.insert(FavoriteContract.MashTemp.TABLE_NAME, null, mashContentValues);
        }

        Ingredients ingredients = beer.getIngredients();
        ContentValues ingredientsContentValues = new ContentValues();
        ingredientsContentValues.put(FavoriteContract.Ingrednient.COLUMN_BEER_ID, beerId);
        ingredientsContentValues.put(FavoriteContract.Ingrednient.COLUMN_YEAST, ingredients.getYeast());

        Long ingredientDbResult = db.insert(FavoriteContract.Ingrednient.TABLE_NAME, null, ingredientsContentValues);
        int ingredientId = ingredientDbResult.intValue();

        List<Malt> malts = ingredients.getMalt();
        ContentValues maltContentValues = new ContentValues();
        for (Malt malt : malts) {
            maltContentValues.put(FavoriteContract.Malt.COLUMN_INGREDIENT_ID, ingredientId);
            maltContentValues.put(FavoriteContract.Malt.COLUMN_NAME, malt.getName());
            maltContentValues.put(FavoriteContract.Malt.COLUMN_AMOUNT, malt.getAmount().getValue());
            maltContentValues.put(FavoriteContract.Malt.COLUMN_AMOUNT_UNIT, malt.getAmount().getUnit());
            db.insert(FavoriteContract.Malt.TABLE_NAME, null, maltContentValues);
        }

        List<Hop> hops = ingredients.getHops();
        ContentValues hopContentValues = new ContentValues();
        for (Hop hop : hops) {
            hopContentValues.put(FavoriteContract.Hop.COLUMN_INGREDIENT_ID, ingredientId);
            hopContentValues.put(FavoriteContract.Hop.COLUMN_NAME, hop.getName());
            hopContentValues.put(FavoriteContract.Hop.COLUMN_ADD, hop.getAdd());
            hopContentValues.put(FavoriteContract.Hop.COLUMN_AMOUNT, hop.getAmount().getValue());
            hopContentValues.put(FavoriteContract.Hop.COLUMN_AMOUNT_UNIT, hop.getAmount().getUnit());
            hopContentValues.put(FavoriteContract.Hop.COLUMN_ATTRIBUTE, hop.getAttribute());
            db.insert(FavoriteContract.Hop.TABLE_NAME, null, hopContentValues);
        }

        List<String> foodPairings = beer.getFoodPairing();
        ContentValues foodPairingValues = new ContentValues();
        for(String foodPairing : foodPairings){
            foodPairingValues.put(FavoriteContract.FoodPairing.COLUMN_BEER_ID, beerId);
            foodPairingValues.put(FavoriteContract.FoodPairing.COLUMN_TEXT, foodPairing);
            db.insert(FavoriteContract.FoodPairing.TABLE_NAME, null, foodPairingValues);
        }

        db.close();
    }

    public void deleteFavoriteBeer(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(FavoriteContract.Beer.TABLE_NAME, FavoriteContract.Beer.COLUMN_SERVER_ID + " = " + id, null);
        db.close();
    }

    public boolean beerExistsInDb(Integer id) {
        SQLiteDatabase db = this.getReadableDatabase();
        String[] columns = { FavoriteContract.Beer.COLUMN_FAVORITE };//not really important which column
        String selection = FavoriteContract.Beer.COLUMN_SERVER_ID + " =?";
        String[] selectionArgs = { id.toString() };
        String limit = "1";

        Cursor cursor = db.query(FavoriteContract.Beer.TABLE_NAME, columns, selection, selectionArgs, null, null, null, limit);
        boolean exists = (cursor.getCount() > 0);
        cursor.close();
        return exists;
    }

    public ArrayList<Beer> getAllFavoriteBeers() {
        String[] beerColumns = {
                FavoriteContract.Beer._ID,
                FavoriteContract.Beer.COLUMN_SERVER_ID,
                FavoriteContract.Beer.COLUMN_NAME,
                FavoriteContract.Beer.COLUMN_TAGLINE,
                FavoriteContract.Beer.COLUMN_FIRST_BREWED,
                FavoriteContract.Beer.COLUMN_DESCRIPTION,
                FavoriteContract.Beer.COLUMN_IMAGE_URL,
                FavoriteContract.Beer.COLUMN_ABV,
                FavoriteContract.Beer.COLUMN_IBU,
                FavoriteContract.Beer.COLUMN_TARGET_FG,
                FavoriteContract.Beer.COLUMN_TARGET_OG,
                FavoriteContract.Beer.COLUMN_EBC,
                FavoriteContract.Beer.COLUMN_SRM,
                FavoriteContract.Beer.COLUMN_PH,
                FavoriteContract.Beer.COLUMN_ATTENUATION_LEVEL,
                FavoriteContract.Beer.COLUMN_VOLUME_TEMPERATURE,
                FavoriteContract.Beer.COLUMN_VOLUME_UNIT,
                FavoriteContract.Beer.COLUMN_BOIL_VOLUME_TEMP,
                FavoriteContract.Beer.COLUMN_BOIL_VOLUME_UNIT,
                FavoriteContract.Beer.COLUMN_BREWERS_TIPS,
                FavoriteContract.Beer.COLUMN_FAVORITE
        };

        String sortOrder =
                FavoriteContract.Beer._ID + " ASC";
        ArrayList<Beer> favoriteBeerList = null;

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(FavoriteContract.Beer.TABLE_NAME,
                beerColumns,
                null,
                null,
                null,
                null,
                sortOrder);

        if (cursor.moveToFirst()) {
            favoriteBeerList = new ArrayList<>();
            do {
                int i = 0;
                Beer beer = new Beer();
                if (!cursor.isNull(i)) beer.setDbId(cursor.getInt(i));
                if (!cursor.isNull(++i)) beer.setId(cursor.getInt(i));
                if (!cursor.isNull(++i)) beer.setName(cursor.getString(i));
                if (!cursor.isNull(++i)) beer.setTagline(cursor.getString(i));
                if (!cursor.isNull(++i)) beer.setFirstBrewed(cursor.getString(i));
                if (!cursor.isNull(++i)) beer.setDescription(cursor.getString(i));
                if (!cursor.isNull(++i)) beer.setImageUrl(cursor.getString(i));
                if (!cursor.isNull(++i)) beer.setAbv(cursor.getDouble(i));
                if (!cursor.isNull(++i)) beer.setIbu(cursor.getDouble(i));
                if (!cursor.isNull(++i)) beer.setTargetFg(cursor.getInt(i));
                if (!cursor.isNull(++i)) beer.setTargetOg(cursor.getDouble(i));
                if (!cursor.isNull(++i)) beer.setEbc(cursor.getDouble(i));
                if (!cursor.isNull(++i)) beer.setSrm(cursor.getDouble(i));
                if (!cursor.isNull(++i)) beer.setPh(cursor.getDouble(i));
                if (!cursor.isNull(++i)) beer.setAttenuationLevel(cursor.getDouble(i));
                Volume volume = null;
                if (!cursor.isNull(++i)) {
                    volume = new Volume();
                    volume.setValue(cursor.getInt(i));
                }
                if (!cursor.isNull(++i)) {
                    if (volume == null) volume = new Volume();
                    volume.setUnit(cursor.getString(i));
                }
                beer.setVolume(volume);
                BoilVolume boilVolume = null;
                if (!cursor.isNull(++i)) {
                    boilVolume = new BoilVolume();
                    boilVolume.setValue(cursor.getInt(i));
                }
                if (!cursor.isNull(++i)) {
                    if (boilVolume == null) boilVolume = new BoilVolume();
                    boilVolume.setUnit(cursor.getString(i));
                }
                beer.setBoilVolume(boilVolume);
                if (!cursor.isNull(++i)) beer.setBrewersTips(cursor.getString(i));
                if (!cursor.isNull(++i)) beer.setFavorite(cursor.getInt(i));
                favoriteBeerList.add(beer);

            } while (cursor.moveToNext());
            cursor.close();

            setMethods(db, favoriteBeerList);
            setIngredients(db, favoriteBeerList);
            setFoodPairings(db, favoriteBeerList);

            if (db.isOpen()) db.close();

        }
        return favoriteBeerList;
    }

    private void setMethods(SQLiteDatabase db, List<Beer> favoriteBeerList) {
        String[] methodColumns = {
                FavoriteContract.Method._ID,
                FavoriteContract.Method.COLUMN_FERMENTATION_TEMP,
                FavoriteContract.Method.COLUMN_TEMP_UNIT,
                FavoriteContract.Method.COLUMN_TWIST
        };

        String[] mashTempColumns = {
                FavoriteContract.MashTemp.COLUMN_MASH_TEMP,
                FavoriteContract.MashTemp.COLUMN_TEMP_UNIT,
                FavoriteContract.MashTemp.COLUMN_DURATION,

        };

        for (Beer beer : favoriteBeerList) {
            Cursor cursor2 = db.query(FavoriteContract.Method.TABLE_NAME,
                    methodColumns,
                    FavoriteContract.Method.COLUMN_BEER_ID + " = " + beer.getDbId(),
                    null,
                    null,
                    null,
                    null);

            Method method = null;
            if (cursor2.moveToFirst()) {
                method = new Method();
                int i = 0;
                if (!cursor2.isNull(i)) method.setDbId(cursor2.getInt(i));
                Fermentation fermentation = null;
                Temp temp = new Temp();
                if (!cursor2.isNull(++i)) {
                    fermentation = new Fermentation();
                    temp = new Temp();
                    temp.setValue(cursor2.getInt(i));
                }
                if (!cursor2.isNull(++i)) {
                    if (fermentation == null) fermentation = new Fermentation();
                    if (temp == null) temp = new Temp();
                    temp.setUnit(cursor2.getString(i));
                }
                fermentation.setTemp(temp);
                method.setFermentation(fermentation);
                if (!cursor2.isNull(++i)) method.setTwist(cursor2.getString(i));
            }
            cursor2.close();

            Cursor cursor3 = db.query(FavoriteContract.MashTemp.TABLE_NAME,
                    mashTempColumns,
                    FavoriteContract.MashTemp.COLUMN_METHOD_ID + " = " + method.getDbId(),
                    null,
                    null,
                    null,
                    null);
            List<MashTemp> mashTemps = null;
            if (cursor3.moveToFirst()) {
                mashTemps = new ArrayList<>();
                do {
                    int i = 0;
                    MashTemp mashTemp = new MashTemp();
                    Temp temp = null;
                    if (!cursor3.isNull(i)) {
                        temp = new Temp();
                        temp.setValue(cursor3.getInt(i));
                    }
                    if (!cursor3.isNull(++i)) {
                        if (temp == null) temp = new Temp();
                        temp.setUnit(cursor3.getString(i));
                    }
                    mashTemp.setTemp(temp);
                    if (!cursor3.isNull(++i)) mashTemp.setDuration(cursor3.getInt(i));
                    mashTemps.add(mashTemp);
                } while (cursor3.moveToNext());
            }
            cursor3.close();
            method.setMashTemp(mashTemps);

            beer.setMethod(method);
        }
    }

    private void setIngredients(SQLiteDatabase db, List<Beer> favoriteBeerList) {
        String[] ingredientColumns = {
                FavoriteContract.Ingrednient._ID,
                FavoriteContract.Ingrednient.COLUMN_YEAST,
        };

        String[] maltColumns = {
                FavoriteContract.Malt.COLUMN_NAME,
                FavoriteContract.Malt.COLUMN_AMOUNT,
                FavoriteContract.Malt.COLUMN_AMOUNT_UNIT,
        };

        String[] hopsColumns = {
                FavoriteContract.Hop.COLUMN_NAME,
                FavoriteContract.Hop.COLUMN_AMOUNT,
                FavoriteContract.Hop.COLUMN_AMOUNT_UNIT,
                FavoriteContract.Hop.COLUMN_ADD,
                FavoriteContract.Hop.COLUMN_ATTRIBUTE,
        };

        for (Beer beer : favoriteBeerList) {
            Cursor cursor4 = db.query(FavoriteContract.Ingrednient.TABLE_NAME,
                    ingredientColumns,
                    FavoriteContract.Ingrednient.COLUMN_BEER_ID + " = " + beer.getDbId(),
                    null,
                    null,
                    null,
                    null);
            Ingredients ingredients = null;
            if (cursor4.moveToFirst()) {
                ingredients = new Ingredients();
                int i = 0;
                if (!cursor4.isNull(i)) ingredients.setDbId(cursor4.getInt(i));
                if (!cursor4.isNull(++i)) ingredients.setYeast(cursor4.getString(i));
            }
            cursor4.close();
            if (ingredients != null) {
                Cursor cursor5 = db.query(FavoriteContract.Malt.TABLE_NAME,
                        maltColumns,
                        FavoriteContract.Malt.COLUMN_INGREDIENT_ID + " = " + ingredients.getDbId(),
                        null,
                        null,
                        null,
                        null);

                List<Malt> malts = null;
                if (cursor5.moveToFirst()) {
                    malts = new ArrayList<>();
                    do {
                        int i = 0;
                        Malt malt = new Malt();
                        if (!cursor5.isNull(i)) malt.setName(cursor5.getString(i));
                        Amount amount = null;
                        if (!cursor5.isNull(++i)) {
                            amount = new Amount();
                            amount.setValue(cursor5.getDouble(i));
                        }
                        if (!cursor5.isNull(++i)) {
                            if (amount == null) amount = new Amount();
                            amount.setUnit(cursor5.getString(i));
                        }
                        malt.setAmount(amount);
                        malts.add(malt);
                    } while (cursor5.moveToNext());

                }

                cursor5.close();

                ingredients.setMalt(malts);

                Cursor cursor6 = db.query(FavoriteContract.Hop.TABLE_NAME,
                        hopsColumns,
                        FavoriteContract.Hop.COLUMN_INGREDIENT_ID + " = " + ingredients.getDbId(),
                        null,
                        null,
                        null,
                        null);

                List<Hop> hops = null;
                if (cursor6.moveToFirst()) {
                    hops = new ArrayList<>();
                    do {
                        int i = 0;
                        Hop hop = new Hop();
                        if (!cursor6.isNull(i)) hop.setName(cursor6.getString(i));
                        Amount amount = null;
                        if (!cursor6.isNull(++i)) {
                            amount = new Amount();
                            amount.setValue(cursor6.getDouble(i));
                        }
                        if (!cursor6.isNull(++i)) {
                            if (amount == null) amount = new Amount();
                            amount.setUnit(cursor6.getString(i));
                        }
                        hop.setAmount(amount);
                        if (!cursor6.isNull(++i)) hop.setAdd(cursor6.getString(i));
                        if (!cursor6.isNull(++i)) hop.setAttribute(cursor6.getString(i));
                        hops.add(hop);
                    } while (cursor6.moveToNext());

                }

                cursor6.close();

                ingredients.setHops(hops);
            }
            beer.setIngredients(ingredients);
        }

    }

    private void setFoodPairings(SQLiteDatabase db, List<Beer> favoriteBeerList) {
        String[] foodPairingsColumns = {
                FavoriteContract.FoodPairing.COLUMN_TEXT,
        };

        for (Beer beer : favoriteBeerList) {
            Cursor cursor7 = db.query(FavoriteContract.FoodPairing.TABLE_NAME,
                    foodPairingsColumns,
                    FavoriteContract.FoodPairing.COLUMN_BEER_ID + " = " + beer.getDbId(),
                    null,
                    null,
                    null,
                    null);

            List<String> foodPairings = null;
            if (cursor7.moveToFirst()) {
                foodPairings = new ArrayList<>();
                do {
                    if (!cursor7.isNull(0)) foodPairings.add(new String(cursor7.getString(0)));
                } while (cursor7.moveToNext());
            }
            beer.setFoodPairing(foodPairings);
            cursor7.close();
        }
    }


    final String SQL_CREATE_BEER_TABLE = "CREATE TABLE " + FavoriteContract.Beer.TABLE_NAME + " (" +
            FavoriteContract.Beer._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            FavoriteContract.Beer.COLUMN_SERVER_ID + " INTEGER, " +
            FavoriteContract.Beer.COLUMN_NAME + " TEXT, " +
            FavoriteContract.Beer.COLUMN_TAGLINE + " TEXT, " +
            FavoriteContract.Beer.COLUMN_FIRST_BREWED + " TEXT, " +
            FavoriteContract.Beer.COLUMN_DESCRIPTION + " TEXT, " +
            FavoriteContract.Beer.COLUMN_IMAGE_URL + " TEXT, " +
            FavoriteContract.Beer.COLUMN_ABV + " REAL, " +
            FavoriteContract.Beer.COLUMN_IBU + " REAL, " +
            FavoriteContract.Beer.COLUMN_TARGET_FG + " INTEGER, " +
            FavoriteContract.Beer.COLUMN_TARGET_OG + " REAL, " +
            FavoriteContract.Beer.COLUMN_EBC + " REAL, " +
            FavoriteContract.Beer.COLUMN_SRM + " REAL, " +
            FavoriteContract.Beer.COLUMN_PH + " REAL, " +
            FavoriteContract.Beer.COLUMN_ATTENUATION_LEVEL + " REAL, " +
            FavoriteContract.Beer.COLUMN_VOLUME_TEMPERATURE + " INTEGER, " +
            FavoriteContract.Beer.COLUMN_VOLUME_UNIT + " TEXT, " +
            FavoriteContract.Beer.COLUMN_BOIL_VOLUME_TEMP + " INTEGER, " +
            FavoriteContract.Beer.COLUMN_BOIL_VOLUME_UNIT + " TEXT, " +
            FavoriteContract.Beer.COLUMN_BREWERS_TIPS + " TEXT, " +
            FavoriteContract.Beer.COLUMN_FAVORITE + " INTEGER" +
            ")";


    final String SQL_CREATE_METHOD_TABLE = "CREATE TABLE " + FavoriteContract.Method.TABLE_NAME + " (" +
            FavoriteContract.Method._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            FavoriteContract.Method.COLUMN_BEER_ID + " INTEGER, " +
            FavoriteContract.Method.COLUMN_FERMENTATION_TEMP + " INTEGER, " +
            FavoriteContract.Method.COLUMN_TEMP_UNIT + " TEXT, " +
            FavoriteContract.Method.COLUMN_TWIST + " TEXT, " +
            "FOREIGN KEY (" + FavoriteContract.Method.COLUMN_BEER_ID + ") REFERENCES " +
            FavoriteContract.Beer.TABLE_NAME + "(" + FavoriteContract.Beer._ID + ") ON DELETE CASCADE" +
            ")";


    final String SQL_CREATE_MASH_TEMP_TABLE = "CREATE TABLE " + FavoriteContract.MashTemp.TABLE_NAME + " (" +
            FavoriteContract.MashTemp._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            FavoriteContract.MashTemp.COLUMN_METHOD_ID + " INTEGER, " +
            FavoriteContract.MashTemp.COLUMN_MASH_TEMP + " INTEGER, " +
            FavoriteContract.MashTemp.COLUMN_TEMP_UNIT + " TEXT, " +
            FavoriteContract.MashTemp.COLUMN_DURATION + " INTEGER, " +
            "FOREIGN KEY (" + FavoriteContract.MashTemp.COLUMN_METHOD_ID + ") REFERENCES " +
            FavoriteContract.Method.TABLE_NAME + "(" + FavoriteContract.Method._ID + ") ON DELETE CASCADE" +
            ")";


    final String SQL_CREATE_INGREDIENT_TABLE = "CREATE TABLE " + FavoriteContract.Ingrednient.TABLE_NAME + " (" +
            FavoriteContract.Ingrednient._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            FavoriteContract.Ingrednient.COLUMN_BEER_ID + " INTEGER, " +
            FavoriteContract.Ingrednient.COLUMN_YEAST + " TEXT, " +
            "FOREIGN KEY (" + FavoriteContract.Ingrednient.COLUMN_BEER_ID + ") REFERENCES " +
            FavoriteContract.Beer.TABLE_NAME + "(" + FavoriteContract.Beer._ID + ") ON DELETE CASCADE" +
            ")";


    final String SQL_CREATE_MALT_TABLE = "CREATE TABLE " + FavoriteContract.Malt.TABLE_NAME + " (" +
            FavoriteContract.Malt._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            FavoriteContract.Malt.COLUMN_INGREDIENT_ID + " INTEGER, " +
            FavoriteContract.Malt.COLUMN_NAME + " TEXT, " +
            FavoriteContract.Malt.COLUMN_AMOUNT + " REAL, " +
            FavoriteContract.Malt.COLUMN_AMOUNT_UNIT + " TEXT, " +
            "FOREIGN KEY (" + FavoriteContract.Malt.COLUMN_INGREDIENT_ID + ") REFERENCES " +
            FavoriteContract.Ingrednient.TABLE_NAME + "(" + FavoriteContract.Ingrednient._ID + ") ON DELETE CASCADE" +
            ")";


    final String SQL_CREATE_HOP_TABLE = "CREATE TABLE " + FavoriteContract.Hop.TABLE_NAME + " (" +
            FavoriteContract.Hop._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            FavoriteContract.Hop.COLUMN_INGREDIENT_ID + " INTEGER, " +
            FavoriteContract.Hop.COLUMN_NAME + " NAME, " +
            FavoriteContract.Hop.COLUMN_AMOUNT + " REAL, " +
            FavoriteContract.Hop.COLUMN_AMOUNT_UNIT + " TEXT, " +
            FavoriteContract.Hop.COLUMN_ADD + " TEXT, " +
            FavoriteContract.Hop.COLUMN_ATTRIBUTE + " TEXT, " +
            "FOREIGN KEY (" + FavoriteContract.Hop.COLUMN_INGREDIENT_ID + ") REFERENCES " +
            FavoriteContract.Ingrednient.TABLE_NAME + "(" + FavoriteContract.Ingrednient._ID + ") ON DELETE CASCADE" +
            ")";


    final String SQL_CREATE_FOOD_PAIRING_TABLE = "CREATE TABLE " + FavoriteContract.FoodPairing.TABLE_NAME + " (" +
            FavoriteContract.FoodPairing._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            FavoriteContract.FoodPairing.COLUMN_BEER_ID + " INTEGER, " +
            FavoriteContract.FoodPairing.COLUMN_TEXT + " TEXT, " +
            "FOREIGN KEY (" + FavoriteContract.FoodPairing.COLUMN_BEER_ID + ") REFERENCES " +
            FavoriteContract.Beer.TABLE_NAME + "(" + FavoriteContract.Beer._ID + ") ON DELETE CASCADE" +
            ")";


}
