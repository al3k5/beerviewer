package com.example.alekskrause.beerviewer.Keys;

/**
 * Created by alekskrause on 21/11/2017.
 */

public class Keys {

    //save instance state keys
    public final static String OUT_UNSORTED_LOCAL_BEERS = "local_unsorted";
    public final static String OUT_UNSORTED_SERVER_BEERS = "server_unsorted";
    public final static String OUT_PAGE_NUMBER = "page_number";
    public final static String OUT_SHOWING_FAVORITE = "showing_favorite";
    public final static String OUT_IS_LOCAL_SORTED = "local_sorted";
    public final static String OUT_IS_SERVER_SORTED = "server_sorted";
    public final static String OUT_BEER = "beer";
    
    
}
