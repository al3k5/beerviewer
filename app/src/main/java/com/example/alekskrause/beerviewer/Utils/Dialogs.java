package com.example.alekskrause.beerviewer.Utils;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.example.alekskrause.beerviewer.R;

/**
 * Created by alekskrause on 20/11/2017.
 */
//inflate a simple dialog
public class Dialogs {
    public static Dialog createAlert(Context context, int stringId, boolean cancelable) {
        String text = context.getString(stringId);
        return createAlert(context, text, cancelable);
    }

    public static Dialog createAlert(Context context, String text, boolean cancelable) {
        Dialog dialog = createBaseDialog(context, R.layout.dialog_alert, cancelable);
        ((TextView) dialog.findViewById(R.id.text)).setText(text);
        dialog.findViewById(R.id.btn_ok).setOnClickListener(newOnClickDismiss(dialog));
        return dialog;
    }

    public static View.OnClickListener newOnClickDismiss(final Dialog dialog) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        };
    }

    public static Dialog createBaseDialog(Context context, int layoutId, boolean cancelable) {
        View dialogView = LayoutInflater.from(context).inflate(layoutId, null);
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView);
        dialog.setCancelable(cancelable);
        return dialog;
    }
}
