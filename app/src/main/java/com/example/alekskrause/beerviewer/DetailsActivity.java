package com.example.alekskrause.beerviewer;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.alekskrause.beerviewer.Keys.Keys;
import com.example.alekskrause.beerviewer.Utils.Device;
import com.example.alekskrause.beerviewer.data.FavoriteDbHelper;
import com.example.alekskrause.beerviewer.model.Amount;
import com.example.alekskrause.beerviewer.model.Beer;
import com.example.alekskrause.beerviewer.model.BoilVolume;
import com.example.alekskrause.beerviewer.model.Fermentation;
import com.example.alekskrause.beerviewer.model.Hop;
import com.example.alekskrause.beerviewer.model.Ingredients;
import com.example.alekskrause.beerviewer.model.Malt;
import com.example.alekskrause.beerviewer.model.MashTemp;
import com.example.alekskrause.beerviewer.model.Method;
import com.example.alekskrause.beerviewer.model.Temp;
import com.example.alekskrause.beerviewer.model.Volume;
import com.github.ivbaranov.mfb.MaterialFavoriteButton;

import java.util.List;

/**
 * Created by alekskrause on 16/11/2017.
 */

public class DetailsActivity extends AppCompatActivity {
    public static final int FAVORITE_CHANGED = 1;
    private ImageView imgThumbnail;

    private TextView textBeerName;
    private TextView textTaglineValue;
    private TextView textFirstBrewedValue;
    private TextView textDescriptionValue;
    private TextView textAbvValue;
    private TextView textIbuValue;
    private TextView textTargetFgValue;
    private TextView textTargetOgValue;
    private TextView textEbcValue;
    private TextView textSrmValue;
    private TextView textPhValue;
    private TextView textAtLevelValue;
    private TextView textVolumeValue;
    private TextView textBoilVolumeValue;
    private TextView textMashValue;
    private TextView textFermTempValue;
    private TextView textFermTwistValue;
    private TextView textMaltValue;
    private TextView textHopsValue;
    private TextView textYeastValue;
    private TextView textFoodPairValue;
    private TextView textBrewersTipsValue;
    private MaterialFavoriteButton materialFavoriteButton;

    private FavoriteDbHelper favoriteDbHelper;
    private Beer beer;
    private boolean wasFavorite;
    private int position;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initCollapsingToolbar();

        imgThumbnail = (ImageView) findViewById(R.id.thumbnail_image_header);
        textBeerName = (TextView) findViewById(R.id.text_beertitle);
        textTaglineValue = (TextView) findViewById(R.id.text_tagline);
        textFirstBrewedValue = (TextView) findViewById(R.id.text_first_brewed_value);
        textDescriptionValue = (TextView) findViewById(R.id.text_description_value);
        textAbvValue = (TextView) findViewById(R.id.text_abv_value);
        textIbuValue = (TextView) findViewById(R.id.text_ibu_value);
        textTargetFgValue = (TextView) findViewById(R.id.text_fg_value);
        textTargetOgValue = (TextView) findViewById(R.id.text_og_value);
        textEbcValue = (TextView) findViewById(R.id.text_ebc_value);
        textSrmValue = (TextView) findViewById(R.id.text_srm_value);
        textPhValue = (TextView) findViewById(R.id.text_ph_value);
        textAtLevelValue = (TextView) findViewById(R.id.text_at_level_value);
        textVolumeValue = (TextView) findViewById(R.id.text_volume_value);
        textBoilVolumeValue = (TextView) findViewById(R.id.text_boil_volume_value);
        textMashValue = (TextView) findViewById(R.id.text_mash_value);
        textFermTempValue = (TextView) findViewById(R.id.text_ferm_temp_value);
        textFermTwistValue = (TextView) findViewById(R.id.text_ferm_twist_value);
        textMaltValue = (TextView) findViewById(R.id.text_malt_value);
        textHopsValue = (TextView) findViewById(R.id.text_hops_value);
        textYeastValue = (TextView) findViewById(R.id.text_yeast_value);
        textFoodPairValue = (TextView) findViewById(R.id.text_food_pairing_value);
        textBrewersTipsValue = (TextView) findViewById(R.id.text_brewers_tips_value);
        materialFavoriteButton = (MaterialFavoriteButton) findViewById(R.id.button_favorite);
        
        Intent intent = getIntent();
        if(intent.hasExtra("beer")){
            beer = getIntent().getExtras().getParcelable("beer");
            materialFavoriteButton.setFavorite(beer.isFavorite());
            wasFavorite = beer.isFavorite();
            position = getIntent().getExtras().getInt("position");

            setTextValues(beer);
            setImgThumbnail(beer.getImageUrl());
            setMaterialFavoriteListener();
        } else {
            imgThumbnail.setImageDrawable(this.getResources().getDrawable(R.drawable.image_missing));
            findViewById(R.id.scroll_view).setVisibility(View.GONE);
            findViewById(R.id.text_something_wrong).setVisibility(View.VISIBLE);
        }

    }
    
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(Keys.OUT_BEER, beer);
        super.onSaveInstanceState(outState);
    }
    
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        beer = savedInstanceState.getParcelable(Keys.OUT_BEER);
        super.onRestoreInstanceState(savedInstanceState);
    }
    
    private void setMaterialFavoriteListener() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        materialFavoriteButton.setOnFavoriteChangeListener(new MaterialFavoriteButton.OnFavoriteChangeListener() {
            @Override
            public void onFavoriteChanged(MaterialFavoriteButton buttonView, boolean favorite) {
                if(favorite){
                    beer.setFavorite(Beer.FAVORITE);
                    saveFavorite();
                    Toast.makeText(DetailsActivity.this, getApplicationContext().getString(R.string.added_to_favorites)
                            , Toast.LENGTH_SHORT).show();
                } else {
                    favoriteDbHelper = new FavoriteDbHelper(getApplicationContext());
                    favoriteDbHelper.deleteFavoriteBeer(beer.getId());
                    beer.setFavorite(Beer.NOT_FAVORITE);
                    Toast.makeText(DetailsActivity.this, getApplicationContext().getString(R.string.removed_from_favorites)
                            , Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //not very pretty code : (
    private void setTextValues(Beer beer) {
        String name = beer.getName();
        if(name!=null)textBeerName.setText(name);
        else{
            textBeerName.setVisibility(View.GONE);
        }

        String tagline = beer.getTagline();
        if(tagline!=null)textTaglineValue.setText(tagline);
        else{
            textTaglineValue.setVisibility(View.GONE);
        }

        String firstBrewed = beer.getFirstBrewed();
        if(firstBrewed!=null)textFirstBrewedValue.setText(firstBrewed);
        else{
            findViewById(R.id.ll_first_brewed).setVisibility(View.GONE);
        }

        String description = beer.getDescription();
        if(description!=null)textDescriptionValue.setText(description);
        else{
            findViewById(R.id.ll_description).setVisibility(View.GONE);
        }

        Double abv = beer.getAbv();
        boolean showAbv = abv!=null;
        Double ibu = beer.getIbu();
        boolean showIbu = ibu!=null;
        Double ebc = beer.getEbc();
        boolean showEbc = ebc!=null;

        if(showAbv || showIbu || showEbc) {
            if (showAbv) textAbvValue.setText(abv.toString());
            else {
                findViewById(R.id.ll_abv).setVisibility(View.GONE);
            }

            if (showIbu) textIbuValue.setText(ibu.toString());
            else {
                findViewById(R.id.ll_ibu).setVisibility(View.GONE);
            }

            if (showEbc) textEbcValue.setText(ebc.toString());
            else {
                findViewById(R.id.ll_ebc).setVisibility(View.GONE);
            }
        } else findViewById(R.id.ll_sort_parameters).setVisibility(View.GONE);


        Integer targetFg = beer.getTargetFg();
        boolean showTargetFg = targetFg!=null;
        Double targetOg = beer.getTargetOg();
        boolean showTargetOg = targetOg!=null;

        if(showTargetFg || showTargetOg) {
            if (showTargetFg) textTargetFgValue.setText(targetFg.toString());
            else {
                findViewById(R.id.ll_target_fg).setVisibility(View.GONE);
            }

            if (showTargetOg) textTargetOgValue.setText(targetOg.toString());
            else {
                findViewById(R.id.ll_target_og).setVisibility(View.GONE);
            }
        } else findViewById(R.id.ll_targets).setVisibility(View.GONE);

        Double srm = beer.getSrm();
        boolean showSrm = srm!=null;
        Double ph = beer.getPh();
        boolean showPh = ph!=null;

        if(showSrm || showPh) {
            if (showSrm) textSrmValue.setText(srm.toString());
            else {
                findViewById(R.id.ll_srm).setVisibility(View.GONE);
            }

            if (showPh) textPhValue.setText(ph.toString());
            else {
                findViewById(R.id.ll_ph).setVisibility(View.GONE);
            }
        } else findViewById(R.id.ll_srm_ph).setVisibility(View.GONE);


        Double attLevel = beer.getAttenuationLevel();
        if(attLevel!=null)textAtLevelValue.setText(attLevel.toString());
        else{
            findViewById(R.id.ll_attenuation_level).setVisibility(View.GONE);
        }

        Volume volume = beer.getVolume();
        if(volume!=null && volume.getUnit()!=null && volume.getValue()!=null){
            textVolumeValue.setText(volume.getValue().toString() + " " +volume.getUnit());
        }
        else{
            findViewById(R.id.ll_volume).setVisibility(View.GONE);
        }

        BoilVolume boilVolume = beer.getBoilVolume();
        if(boilVolume!=null && boilVolume.getUnit()!=null && boilVolume.getValue()!=null){
            textBoilVolumeValue.setText(boilVolume.getValue().toString() + " " + boilVolume.getUnit());
        }
        else{
            findViewById(R.id.ll_boil_volume).setVisibility(View.GONE);
        }

        Method method = beer.getMethod();
        if(method!=null) {
            List<MashTemp> mashTemps = method.getMashTemp();
            String mashString = null;
            if (mashTemps != null) {
                for (MashTemp mashTemp : mashTemps) {
                    if(mashString!=null) mashString = mashString + "\n";
                    String mash = null;
                    Temp temp = mashTemp.getTemp();
                    if (temp != null) {
                        Integer tempValue = temp.getValue();
                        String unit = temp.getUnit();
                        if (tempValue != null && unit != null)
                            mash = "Mash at " + tempValue.toString() +" " + unit;
                    }

                    Integer duration = mashTemp.getDuration();
                    if (duration != null && mash != null)
                        mash = mash + " for a duration of " + duration.toString() + " units";
                    if (mash != null) mashString = mash;
                }
            }
            if (mashString != null) textMashValue.setText(mashString);
            else findViewById(R.id.ll_mash).setVisibility(View.GONE);

            Fermentation fermentation = method.getFermentation();
            String fermString = null;
            if (fermentation != null) {
                Temp temp = fermentation.getTemp();
                if(temp!=null){
                    Integer fermTempValue = temp.getValue();
                    String fermUnit = temp.getUnit();
                    if(fermTempValue!=null && fermUnit!=null) fermString = fermTempValue.toString() + " " + fermUnit;
                }
            }
            if(fermString!=null) textFermTempValue.setText(fermString);
            else findViewById(R.id.ll_fermentation_temp).setVisibility(View.GONE);

            String twist = method.getTwist();
            if(twist!=null) textFermTwistValue.setText(twist);
            else  textFermTwistValue.setVisibility(View.GONE);
        }
        else{
            findViewById(R.id.ll_making_method).setVisibility(View.GONE);
        }

        Ingredients ingredients = beer.getIngredients();
        if(ingredients!=null){
            String maltString = null;
            List<Malt> malts = ingredients.getMalt();
            if(malts!=null){
                for(Malt malt : malts){
                    if(maltString!=null) maltString = maltString + "\n";
                    String maltName = malt.getName();
                    if(maltName!=null) {
                        if(maltString!=null)maltString= maltString + maltName + " ";
                        else maltString = maltName + " ";
                    }
                    Amount amount = malt.getAmount();
                    if(amount!=null){
                        Double value = amount.getValue();
                        String unit = amount.getUnit();
                        if(value!= null && unit!= null) maltString=maltString + value + unit + " ";
                    }
                }
            }
            if(maltString!=null) textMaltValue.setText(maltString);
            else findViewById(R.id.ll_malt).setVisibility(View.GONE);

            String hopString = null;
            List<Hop> hops = ingredients.getHops();
            if(hops!=null){
                for(Hop hop : hops){
                    if(hopString!=null) hopString = hopString + "\n";
                    String hopName = hop.getName();
                    if(hopName!=null){
                        if(hopString!=null)hopString= hopString + hopName + " ";
                        else hopString = hopName + " ";
                    }
                    Amount amount = hop.getAmount();
                    if(amount!=null){
                        Double value = amount.getValue();
                        String unit = amount.getUnit();
                        if(value!= null && unit!= null) hopString=hopString + value + unit + " ";
                    }
                    String add = hop.getAdd();
                    if(add!=null) hopString = hopString + "add at " + add + " ";
                    String attribute = hop.getAttribute();
                    if(attribute!=null) hopString = hopString + " (attribute: " + attribute + ")";
                }
            }
            if(hopString!=null) textHopsValue.setText(hopString);
            else findViewById(R.id.ll_hops).setVisibility(View.GONE);

            String yeast = ingredients.getYeast();
            if(yeast!=null) textYeastValue.setText(yeast);
            else findViewById(R.id.ll_yeast).setVisibility(View.GONE);
        } else findViewById(R.id.ll_ingredients).setVisibility(View.GONE);

        List<String> foodPairings = beer.getFoodPairing();
        String foodPairingString = null;
        if(foodPairings!=null){
            for(String foodPairing : foodPairings){
                if(foodPairingString!=null) {
                    foodPairingString=foodPairingString + "\n";
                    foodPairingString = foodPairingString + foodPairing;
                } else foodPairingString = foodPairing;
            }
        }
        if(foodPairingString!=null) textFoodPairValue.setText(foodPairingString);
        else findViewById(R.id.ll_food_pairing).setVisibility(View.GONE);

        String brewersTips = beer.getBrewersTips();
        if(brewersTips!=null) textBrewersTipsValue.setText(brewersTips);
        else findViewById(R.id.ll_brewers_tips).setVisibility(View.GONE);

    }

    private void setImgThumbnail(String imageUrl) {
        if(imageUrl!=null) {
            //set image size according to screen ratio. Fixes bug where placeholder takes over image size.
            Display display = Device.getDisplay(getApplicationContext());
            Point size = new Point();
            display.getSize(size);
            int placeholderWidth = size.x / 2;
            int placeholderHeight = size.x * 3 / 2;
            Glide.with(this)
                    .load(imageUrl)
                    .error(R.drawable.image_missing)
                    .dontAnimate()
                    .placeholder(R.drawable.load_beer)
                    .override(placeholderWidth, placeholderHeight)
                    .into(imgThumbnail);
        } else imgThumbnail.setImageDrawable
                (getApplicationContext().getResources().getDrawable(R.drawable.image_missing));
    }

    private void initCollapsingToolbar(){
        final CollapsingToolbarLayout collapsingToolbarLayout =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle("");
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        appBarLayout.setExpanded(true);

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean showing = false;
            int scrollRange = -1;
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if(scrollRange == -1){
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if(scrollRange + verticalOffset == 0){
                    collapsingToolbarLayout.setTitle(getString(R.string.beer_details));
                    showing=true;
                } else if(showing){
                    collapsingToolbarLayout.setTitle(" ");
                    showing = false;
                }
            }
        });
    }
    
    

    public void saveFavorite(){
        favoriteDbHelper = new FavoriteDbHelper(getApplicationContext());
        favoriteDbHelper.addFavouriteBeer(beer);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(wasFavorite!=beer.isFavorite()) {
            Intent intent = new Intent();
            intent.putExtra("position", position);
            beer.setFavorite(wasFavorite);
            intent.putExtra("beer", beer);
            setResult(FAVORITE_CHANGED, intent);
        }
        super.onBackPressed();
    }
}
