package com.example.alekskrause.beerviewer;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.alekskrause.beerviewer.Keys.Keys;
import com.example.alekskrause.beerviewer.Utils.Connectivity;
import com.example.alekskrause.beerviewer.Utils.Device;
import com.example.alekskrause.beerviewer.Utils.Dialogs;
import com.example.alekskrause.beerviewer.adapter.BeersAdapter;
import com.example.alekskrause.beerviewer.adapter.OnBottomReachedListener;
import com.example.alekskrause.beerviewer.data.AsyncTaskDB;
import com.example.alekskrause.beerviewer.model.Beer;
import com.example.alekskrause.beerviewer.networking.BeerClient;
import com.example.alekskrause.beerviewer.networking.BeerService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private BeersAdapter serverAdapter;
    private BeersAdapter localAdapter;

    private SwipeRefreshLayout swipeContainer;
    private AppCompatActivity activity = MainActivity.this;
    private boolean showingFavorites = false;
    int cacheSize = 10 * 1024 * 1024;//10MB
    private int pageNumber = 1;
    public static final int INTENT_DETAIL = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.content_main);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        //server and api adapters
        serverAdapter = new BeersAdapter(this);
        localAdapter = new BeersAdapter(this);


        if (getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        } else {
            int spanCount;
            if (Device.isMobile(getApplicationContext())) spanCount = 3;
            else spanCount = 4;
            recyclerView.setLayoutManager(new GridLayoutManager(this, spanCount));
        }
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        serverAdapter.setOnBottomReachedListener(new OnBottomReachedListener() {
            @Override
            public void onBottomReached(int position) {
                pageNumber++;
                loadJSON();
                swipeContainer.setRefreshing(true);
            }
        });

        swipeContainer.setColorSchemeResources(android.R.color.holo_orange_dark);


        initServerViews();
        if (savedInstanceState == null) loadJSON();
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(Keys.OUT_UNSORTED_LOCAL_BEERS, localAdapter.getUnSortedBeers());

        outState.putParcelableArrayList(Keys.OUT_UNSORTED_SERVER_BEERS, serverAdapter.getUnSortedBeers());

        outState.putInt(Keys.OUT_PAGE_NUMBER, pageNumber);
        outState.putBoolean(Keys.OUT_SHOWING_FAVORITE, showingFavorites);
        outState.putBoolean(Keys.OUT_IS_LOCAL_SORTED, localAdapter.isSorted());
        outState.putBoolean(Keys.OUT_IS_SERVER_SORTED, serverAdapter.isSorted());
        super.onSaveInstanceState(outState);


    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        showingFavorites = savedInstanceState.getBoolean(Keys.OUT_SHOWING_FAVORITE, false);
        pageNumber = savedInstanceState.getInt(Keys.OUT_PAGE_NUMBER, pageNumber);
        ArrayList<Beer> unSortedLocalBeers = savedInstanceState.getParcelableArrayList(Keys.OUT_UNSORTED_LOCAL_BEERS);
        if (unSortedLocalBeers != null) localAdapter.setBeers(unSortedLocalBeers);
        boolean localSorted = savedInstanceState.getBoolean(Keys.OUT_IS_LOCAL_SORTED);
        localAdapter.setSorted(!localSorted);//opposite value. Changed back when sort done once again

        ArrayList<Beer> unSortedServerBeers = savedInstanceState.getParcelableArrayList(Keys.OUT_UNSORTED_SERVER_BEERS);
        if (unSortedServerBeers != null) serverAdapter.setBeers(unSortedServerBeers);
        boolean serverSorted = savedInstanceState.getBoolean(Keys.OUT_IS_SERVER_SORTED);
        serverAdapter.setSorted(!serverSorted);//opposite value. Changed back when sort done once again

        changeOrderOfAllBeers();

        if (showingFavorites) {
            initDbViews();
            localAdapter.notifyDataSetChanged();
        } else {
            initServerViews();
            serverAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == INTENT_DETAIL) {
            if (resultCode == DetailsActivity.FAVORITE_CHANGED) {
                int position = data.getIntExtra("position", -1);
                if (position != -1 && data.hasExtra("beer")) {
                    Beer beer = data.getExtras().getParcelable("beer");
                    if (showingFavorites) {//favorite removed
                        localAdapter.removeBeer(position);
                    } else { //favorite added
                        List<Beer> beers = new ArrayList<>();
                        beers.add(beer);
                        if (localAdapter.hasBeers()) localAdapter.addToBeers(beers);
                        if(localAdapter.isSorted())localAdapter.sortBeers(getSortBy(), getSortOrder());
                    }
                }
            }
        }
    }


    public Activity getActivity() {
        Context context = this;
        while (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                return (Activity) context;
            }
            context = ((ContextWrapper) context).getBaseContext();
        }
        return null;
    }

    //all data comes from server
    private void initServerViews() {
        showingFavorites = false;
        recyclerView.setAdapter(serverAdapter);
        serverAdapter.notifyDataSetChanged();
        swipeContainer.setEnabled(false);
    }

    //For favorites. All data comes from database
    private void initDbViews() {
        showingFavorites = true;
        recyclerView.setAdapter(localAdapter);
        localAdapter.notifyDataSetChanged();
        swipeContainer.setEnabled(false);
        if (!localAdapter.hasBeers()) getAllFavoriteBeers();
    }

    //gets json from server and stores in okhttp cache for offline
    private void loadJSON() {
        try {
            Cache cache = new Cache(getCacheDir(), cacheSize);

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .cache(cache)
                    .addInterceptor(new Interceptor() {
                        @Override
                        public okhttp3.Response intercept(Chain chain) throws IOException {
                            Request request = chain.request();
                            if (!Connectivity.isConnected(getApplicationContext())) {
                                int maxStale = 60 * 60 * 24 * 28;
                                request = request.newBuilder()
                                        .header("Cache-Control", "public, only-if-cached, max-stale=" + maxStale)
                                        .build();
                            }
                            return chain.proceed(request);
                        }
                    }).build();

            Retrofit.Builder builder = new Retrofit.Builder()
                    .baseUrl(BeerClient.API_PATH)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create());

            Retrofit retrofit = builder.build();
            BeerService beerService = retrofit.create(BeerService.class);
            Call<List<Beer>> call = beerService.getBeers(pageNumber);
            call.enqueue(new Callback<List<Beer>>() {
                @Override
                public void onResponse(Call<List<Beer>> call, Response<List<Beer>> response) {
                    List<Beer> beerList = response.body();
                    if (beerList != null && !beerList.isEmpty()) {
                        if (serverAdapter.hasBeers()) {
                            serverAdapter.addToBeers(beerList);
                        } else {
                            serverAdapter.setBeers(beerList);
                            serverAdapter.notifyDataSetChanged();
                        }
                    }
                    if (swipeContainer.isRefreshing()) {
                        swipeContainer.setRefreshing(false);
                    }
                }

                @Override
                public void onFailure(Call<List<Beer>> call, Throwable t) {
                    Log.d("Error", t.getMessage());
                    Toast.makeText(MainActivity.this, getApplicationContext().getString(R.string.error_retrieving_data)
                            , Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void changeOrderOfAllBeers() {
        changeOrderOfBeers(showingFavorites, false);//locals
        changeOrderOfBeers(!showingFavorites, false);//server
    }

    private String getSortBy() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        String sortBy = preferences.getString(
                this.getString(R.string.pref_sort_by_key),
                this.getString(R.string.abv));
        return sortBy;
    }

    private String getSortOrder(){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        String sortOrder = preferences.getString(
                this.getString(R.string.pref_sort_order_key),
                this.getString(R.string.pref_ascending));
        return sortOrder;
    }


    private void changeOrderOfBeers(boolean favorites, boolean withToast) {

        String sortBy = getSortBy();

        String sortOrder = getSortOrder();

        boolean sorted;
        if (favorites) {
            localAdapter.sort(sortBy, sortOrder);
            sorted = localAdapter.isSorted();
            localAdapter.notifyDataSetChanged();
        } else {
            serverAdapter.sort(sortBy, sortOrder);
            sorted = serverAdapter.isSorted();
            serverAdapter.notifyDataSetChanged();
        }
        if (withToast) {
            if (sorted)
                Toast.makeText(activity, getApplicationContext().getString(R.string.beers_sorted), Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(activity, getApplicationContext().getString(R.string.beers_unsorted), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sort_settings:
                Intent intent = new Intent(this, SortSettingsActivity.class);
                startActivity(intent);
                return true;
            case R.id.show_all_beers:
                if (showingFavorites) {
                    initServerViews();
                    Toast.makeText(activity, getApplicationContext().getString(R.string.showing_all_beers), Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(activity, getApplicationContext().getString(R.string.already_showing_all), Toast.LENGTH_SHORT).show();
                return true;
            case R.id.show_favorites:
                if (!showingFavorites) {
                    initDbViews();
                    Toast.makeText(activity, getApplicationContext().getString(R.string.showing_favorite_beers), Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(activity, getApplicationContext().getString(R.string.already_showing_favs), Toast.LENGTH_SHORT).show();
                return true;
            case R.id.sort_beers:
                changeOrderOfBeers(showingFavorites, true);
                return true;
            case R.id.help:
                Dialogs.createAlert(this, R.string.help_text, true).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //fetches favorite beers from db
    class OnDatabaseQueryFinishedListener implements AsyncTaskDB.OnFinishListener {

        @Override
        public void onFinished(ArrayList<Beer> beerList) {
            if (beerList != null) {
                localAdapter.setBeers(beerList);
                localAdapter.notifyDataSetChanged();
            }
            if (swipeContainer.isRefreshing()) {
                swipeContainer.setRefreshing(false);
            }
            localAdapter.setBeersSet(true);
        }
    }

    private void getAllFavoriteBeers() {
        new AsyncTaskDB(getApplicationContext(), new OnDatabaseQueryFinishedListener()).execute();
    }
}
