
package com.example.alekskrause.beerviewer.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BoilVolume implements Parcelable
{

    @SerializedName("value")
    @Expose
    private Integer value;
    @SerializedName("unit")
    @Expose
    private String unit;
    public final static Parcelable.Creator<BoilVolume> CREATOR = new Creator<BoilVolume>() {


        @SuppressWarnings({
            "unchecked"
        })
        public BoilVolume createFromParcel(Parcel in) {
            return new BoilVolume(in);
        }

        public BoilVolume[] newArray(int size) {
            return (new BoilVolume[size]);
        }

    }
    ;

    protected BoilVolume(Parcel in) {
        this.value = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.unit = ((String) in.readValue((String.class.getClassLoader())));
    }

    public BoilVolume() {
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(value);
        dest.writeValue(unit);
    }

    public int describeContents() {
        return  0;
    }

}
