package com.example.alekskrause.beerviewer.data;

import android.provider.BaseColumns;

/**
 * Created by alekskrause on 19/11/2017.
 * Holds all SQLITE Table names and columns
 */

public class FavoriteContract {

    public static final class Beer implements BaseColumns{
        public static final String TABLE_NAME = "favorite_beer";
        public static final String COLUMN_SERVER_ID = "serverId";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_TAGLINE = "tagLine";
        public static final String COLUMN_FIRST_BREWED = "firstBrewed";
        public static final String COLUMN_DESCRIPTION = "description";
        public static final String COLUMN_IMAGE_URL = "imageUrl";
        public static final String COLUMN_ABV = "abv";
        public static final String COLUMN_IBU = "ibu";
        public static final String COLUMN_TARGET_FG = "targetFG";
        public static final String COLUMN_TARGET_OG = "targetOG";
        public static final String COLUMN_EBC = "ebc";
        public static final String COLUMN_SRM = "srm";
        public static final String COLUMN_PH = "ph";
        public static final String COLUMN_ATTENUATION_LEVEL = "attenuationLevel";
        public static final String COLUMN_VOLUME_TEMPERATURE = "volumeTemperature";
        public static final String COLUMN_VOLUME_UNIT = "volUnit";
        public static final String COLUMN_BOIL_VOLUME_UNIT = "boilVolUnit";
        public static final String COLUMN_BOIL_VOLUME_TEMP = "boilVolumeTemp";
        public static final String COLUMN_BREWERS_TIPS = "brewersTips";
        public static final String COLUMN_FAVORITE = "favorite";

    }

    //method has all values except for mashTemps
    public static final class Method implements BaseColumns{
        public static final String TABLE_NAME = "method";
        public static final String COLUMN_BEER_ID = "beer_id";
        public static final String COLUMN_FERMENTATION_TEMP = "fermentationTemperature";
        public static final String COLUMN_TEMP_UNIT = "temp_unit";
        public static final String COLUMN_TWIST = "twist";
    }
    //goes with method
    public static final class MashTemp implements BaseColumns{
        public static final String TABLE_NAME = "mashTemp";
        public static final String COLUMN_METHOD_ID = "methodId";
        public static final String COLUMN_MASH_TEMP = "mashTemp";
        public static final String COLUMN_TEMP_UNIT = "temp_unit";
        public static final String COLUMN_DURATION = "duration";
    }



    //could maybe have done without the ingredient table
    public static final class Ingrednient implements BaseColumns{
        public static final String TABLE_NAME = "ingredient";
        public static final String COLUMN_BEER_ID = "beerId";
        public static final String COLUMN_YEAST = "yeast";
    }
    public static final class Malt implements BaseColumns{
        public static final String TABLE_NAME = "malt";
        public static final String COLUMN_INGREDIENT_ID = "ingredientId";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_AMOUNT = "amount";
        public static final String COLUMN_AMOUNT_UNIT = "amountUnit";
    }
    public static final class Hop implements BaseColumns{
        public static final String TABLE_NAME = "hop";
        public static final String COLUMN_INGREDIENT_ID = "ingredientId";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_AMOUNT = "amount";
        public static final String COLUMN_AMOUNT_UNIT = "amountUnit";
        public static final String COLUMN_ADD = "_add";
        public static final String COLUMN_ATTRIBUTE = "attribute";
    }


    public static final class FoodPairing implements BaseColumns{
        public static final String TABLE_NAME = "foodPairing";
        public static final String COLUMN_BEER_ID = "beerId";
        public static final String COLUMN_TEXT = "name";
    }

}
