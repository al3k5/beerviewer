package com.example.alekskrause.beerviewer.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.alekskrause.beerviewer.DetailsActivity;
import com.example.alekskrause.beerviewer.MainActivity;
import com.example.alekskrause.beerviewer.R;
import com.example.alekskrause.beerviewer.Utils.Device;
import com.example.alekskrause.beerviewer.data.FavoriteDbHelper;
import com.example.alekskrause.beerviewer.model.Beer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by alekskrause on 16/11/2017.
 */

public class BeersAdapter extends RecyclerView.Adapter<BeersAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<Beer> beers = new ArrayList<>();
    private ArrayList<Beer> unSortedBeers = new ArrayList<>();
    private boolean isSorted = false;
    private boolean beersSet = false;

    private OnBottomReachedListener onBottomReachedListener;

    public BeersAdapter(Context context) {
        this.context = context;
    }

    @Override
    public BeersAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.beer_card, parent, false);

        return new MyViewHolder(view);
    }

    //set card views
    @Override
    public void onBindViewHolder(BeersAdapter.MyViewHolder holder, int position) {
        holder.textBeerName.setVisibility(View.VISIBLE);
        holder.textAbvValue.setVisibility(View.VISIBLE);
        holder.textAbv.setVisibility(View.VISIBLE);
        holder.textIbuValue.setVisibility(View.VISIBLE);
        holder.textIbu.setVisibility(View.VISIBLE);
        holder.textEbc.setVisibility(View.VISIBLE);
        holder.textEbcValue.setVisibility(View.VISIBLE);
    
        if (position == beers.size() - 1){
            if(onBottomReachedListener!=null)onBottomReachedListener.onBottomReached(position);
        }
        String name = beers.get(position).getName();
        if (name != null) holder.textBeerName.setText(name);
        else {
            holder.textBeerName.setVisibility(View.GONE);
        }


        Double abv = beers.get(position).getAbv();
        if (abv != null) holder.textAbvValue.setText(abv.toString());
        else {
            holder.textAbvValue.setVisibility(View.GONE);
            holder.textAbv.setVisibility(View.GONE);
        }

        Double ibu = beers.get(position).getIbu();
        if (ibu != null) holder.textIbuValue.setText(ibu.toString());
        else {
            holder.textIbuValue.setVisibility(View.GONE);
            holder.textIbu.setVisibility(View.GONE);
        }


        Double ebc = beers.get(position).getEbc();
        if (ebc != null) holder.textEbcValue.setText(ebc.toString());
        else {
            holder.textEbcValue.setVisibility(View.GONE);
            holder.textEbc.setVisibility(View.GONE);
        }

        String imageUrl = beers.get(position).getImageUrl();
        if (imageUrl != null) {
            setImage(holder, imageUrl);
        } else {
            holder.imgThumbnail.setImageDrawable(context.getResources().getDrawable(R.drawable.image_missing));
        }
    }

    private void setImage(MyViewHolder holder, String imageUrl) {
        //set image size according to screen ratio. Fixes bug where placeholder takes over image size
        Display display = Device.getDisplay(context);
        Point size = new Point();
        display.getSize(size);
        int placeholderWidth = size.x / 2;
        int placeholderHeight = size.x * 3 / 2;
        Glide.with(context)
                .load(imageUrl)
                .error(R.drawable.image_missing)
                .dontAnimate()
                .placeholder(R.drawable.load_beer)
                .override(placeholderWidth, placeholderHeight)
                .into(holder.imgThumbnail);
    }


    @Override
    public int getItemCount() {
        if (beers == null) return 0;
        return beers.size();
    }

    public void addToBeers(List<Beer> beerList) {
        int rangeStart = this.beers.size()-1;
        int rangeEnd = rangeStart + beerList.size()-1;
        unSortedBeers.addAll(beerList);
        this.beers.addAll(beerList);
        notifyItemRangeChanged(rangeStart, rangeEnd);
    }

    public boolean hasBeers(){
        if(this.beers==null || this.beers.isEmpty())return false;
        else return true;
    }

    public void setBeers(List<Beer> beerList) {
        this.beers = new ArrayList<>(beerList);
        this.unSortedBeers = new ArrayList<>(beerList);
    }

    public ArrayList<Beer> getBeers() {
        return beers;
    }

    public ArrayList<Beer> getUnSortedBeers() {
        return unSortedBeers;
    }

    public void setSorted(boolean sorted) {
        isSorted = sorted;
    }

    public void clearBeers() {
        this.beers.clear();
        this.unSortedBeers.clear();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView textBeerName;
        public ImageView imgThumbnail;
        public TextView textAbv;
        public TextView textAbvValue;
        public TextView textIbu;
        public TextView textIbuValue;
        public TextView textEbc;
        public TextView textEbcValue;

        public MyViewHolder(View view) {
            super(view);
            textBeerName = (TextView) view.findViewById(R.id.text_beer_name);
            imgThumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            textAbv = (TextView) view.findViewById(R.id.text_abv);
            textAbvValue = (TextView) view.findViewById(R.id.text_abv_value);
            textIbu = (TextView) view.findViewById(R.id.text_ibu);
            textIbuValue = (TextView) view.findViewById(R.id.text_ibu_value);
            textEbc = (TextView) view.findViewById(R.id.text_ebc);
            textEbcValue = (TextView) view.findViewById(R.id.text_ebc_value);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        Beer clickedDataItem = beers.get(position);
                        Intent intent = new Intent(context, DetailsActivity.class);
                        FavoriteDbHelper favoriteDbHelper = new FavoriteDbHelper(context);
                        boolean isFavorite = favoriteDbHelper.beerExistsInDb(clickedDataItem.getId());

                        clickedDataItem.setFavorite(isFavorite);
                        intent.putExtra("beer", clickedDataItem);
                        intent.putExtra("position", position);
                        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        ((MainActivity)context).startActivityForResult(intent, MainActivity.INTENT_DETAIL);
                    }
                }
            });
        }
    }

    public void sort(String sortBy, String sortOrder){
        if(isSorted)unSortBeers();
        else sortBeers(sortBy, sortOrder);
    }
    public boolean isSorted(){
        return this.isSorted;
    }

    public void unSortBeers(){
        beers = new ArrayList<>(unSortedBeers);
        isSorted = false;
    }

    public void sortBeers(String sortBy, String sortOrder) {
        if(beers==null || beers.isEmpty()) return;
        if(beers.size()> unSortedBeers.size()){
            for(int i= unSortedBeers.size()-1;i<beers.size();i++){
                unSortedBeers.add(beers.get(i));
            }
        }

        if (sortBy.equals(context.getString(R.string.abv))) {
            if (sortOrder.equals(context.getString(R.string.pref_ascending)))
                Collections.sort(beers, Beer.BY_ABV_ASC);
            else Collections.sort(beers, Beer.BY_ABV_DESC);
        } else if (sortBy.equals(context.getString(R.string.ebc))) {
            if (sortOrder.equals(context.getString(R.string.pref_ascending)))
                Collections.sort(beers, Beer.BY_EBC_ASC);
            else Collections.sort(beers, Beer.BY_EBC_DESC);
        } else if (sortBy.equals(context.getString(R.string.ibu))) {
            if (sortOrder.equals(context.getString(R.string.pref_ascending)))
                Collections.sort(beers, Beer.BY_IBU_ASC);
            else Collections.sort(beers, Beer.BY_IBU_DESC);
        } else throw new IllegalStateException("Unknown sortBy" + sortBy);

        isSorted = true;
    }

    public boolean isBeersSet() {
        return beersSet;
    }

    public void setBeersSet(boolean beersSet) {
        this.beersSet = beersSet;
    }

    public void removeBeer(int position){
        beers.remove(position);
        unSortedBeers.remove(position);
        notifyItemRemoved(position);
    }

    //callback to main menu
    public void setOnBottomReachedListener(OnBottomReachedListener onBottomReachedListener){
        this.onBottomReachedListener = onBottomReachedListener;
    }
}
