
package com.example.alekskrause.beerviewer.model;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Method implements Parcelable
{
    private Integer dbId;
    @SerializedName("mash_temp")
    @Expose
    private List<MashTemp> mashTemp = null;
    @SerializedName("fermentation")
    @Expose
    private Fermentation fermentation;
    @SerializedName("twist")
    @Expose
    private String twist;
    public final static Parcelable.Creator<Method> CREATOR = new Creator<Method>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Method createFromParcel(Parcel in) {
            return new Method(in);
        }

        public Method[] newArray(int size) {
            return (new Method[size]);
        }

    }
    ;

    protected Method(Parcel in) {
        this.mashTemp = new ArrayList<>();
        in.readList(this.mashTemp, (com.example.alekskrause.beerviewer.model.MashTemp.class.getClassLoader()));
        this.fermentation = ((Fermentation) in.readValue((Fermentation.class.getClassLoader())));
        this.twist = (String) in.readValue((String.class.getClassLoader()));
        this.dbId = ((Integer) in.readValue((Integer.class.getClassLoader())));
    }

    public Method() {
    }

    public List<MashTemp> getMashTemp() {
        return mashTemp;
    }

    public void setMashTemp(List<MashTemp> mashTemp) {
        this.mashTemp = mashTemp;
    }

    public Fermentation getFermentation() {
        return fermentation;
    }

    public void setFermentation(Fermentation fermentation) {
        this.fermentation = fermentation;
    }

    public String getTwist() {
        return twist;
    }

    public void setTwist(String twist) {
        this.twist = twist;
    }


    public Integer getDbId() {
        return dbId;
    }

    public void setDbId(Integer dbId) {
        this.dbId = dbId;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(mashTemp);
        dest.writeValue(fermentation);
        dest.writeValue(twist);
        dest.writeValue(dbId);
    }

    public int describeContents() {
        return  0;
    }

}
