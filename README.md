### What is this repository for? ###
This is an android native application built on the PUNK API: https://punkapi.com/documentation/v2.
At the moment this is the one and only version for this application.

### How do I get set up? ###

Git clone to your computer and run with android studio. Created with android studio 3.0.